<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Item;
use AppBundle\Form\ItemType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class TodoController extends Controller
{
    /**
     * @Route("/", name="homepage")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
	    $message = '';

	    // Initialize the task instance
        $task = new Item();

	    $form = $this->createForm(ItemType::class, $task);

	    $form->handleRequest($request);

	    if ($form->isSubmitted()) {

		    if ($form->get('cancel')->isClicked()) {

			    // Empty the task, so there will be an empty form
			    $task->initialize();
			    return $this->redirectToRoute('homepage');

		    } elseif ($form->get('save')->isClicked() && $form->isValid()) {

		        // Get the submitted data into the task
		        $task = $form->getData();

		        // Save the task to the database
		        $em = $this->getDoctrine()->getManager();
		        $em->persist($task);
		        $em->flush();

			    // Empty the task, so there will be an empty form
			    $task->initialize();
			    return $this->redirectToRoute('homepage');
	        }
	    }

	    $items = $this->getDoctrine()
		    ->getRepository('AppBundle:Item')
		    ->findAll();

        return $this->render('sf2todo/list.html.twig', array(
            'form' => $form->createView(),
            'items' => $items,
	        'message' => $message,
        ));

    }


    /**
     * Matches /removeitem/*
     *
	 * @Route("/removeitem/{id}", name="remove_item", requirements={"id": "\d+"})
	 *
	 * @param int $id
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
    public function removeAction($id = 0)
    {
	    $message = '';

	    $repository = $this->getDoctrine()->getRepository('AppBundle:Item');

	    $item = $repository->find($id);

	    if (!$item) {
		    $message = 'Todo item not found.';

	    } else {

		    // Remove the item from the database
		    $em = $this->getDoctrine()->getManager();
		    $em->remove($item);
		    $em->flush();

	    }

	    // Initialize the task instance
	    $item = new Item();
	    $form = $this->createForm(ItemType::class, $item);

	    $items = $repository->findAll();

	    return $this->render('sf2todo/list.html.twig', array(
		    'form' => $form->createView(),
		    'items' => $items,
		    'message' => $message,
	    ));
    }

	/**
	 * Matches /edititem/*
	 *
	 * @Route("/edititem/{id}", name="edit_item", requirements={"id": "\d+"})
	 *
	 * @param int $id
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function editAction($id = 0, Request $request)
	{
		$message = '';

		$em = $this->getDoctrine()->getManager();

		$item = $em->getRepository('AppBundle:Item')->find($id);

		if (!$item) {
			$message = 'Todo item not found.';

			// Initialize the task instance
			$item = new Item();

			$form = $this->createForm(ItemType::class, $item);

		} else {

			$form = $this->createForm(ItemType::class, $item);

			$form->handleRequest($request);

			if ($form->isSubmitted()) {

				if ($form->get('cancel')->isClicked()) {

					return $this->redirectToRoute('homepage');

				} elseif ($form->get('save')->isClicked() && $form->isValid()) {

					// Get the submitted data into the task
					$item = $form->getData();

					$em->flush();

					// Empty the task, so there will be an empty form
					$item->initialize();

					return $this->redirectToRoute('homepage');
				}

			}

		}

		$items = $em->getRepository('AppBundle:Item')->findAll();

		return $this->render('sf2todo/list.html.twig', array(
			'form' => $form->createView(),
			'items' => $items,
			'message' => $message,
		));
	}

	/**
     * Matches /list/*
     *
     * @Route("/list/{status}", name="todo_list_status")
     *
     * @param $status
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listStatusAction($status)
    {
        // $status will equal the dynamic part of the URL
        // e.g. at /list/open-routing, then $status='open-routing'

        // ...
        return $this->render('sf2todo/list.html.twig', array(
            'status' => $status,
        ));
    }

}
