<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="task")
 */
class Item
{
	/**
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @ORM\Column(type="string", length=100)
	 *
	 * @Assert\NotBlank()
	 */
    protected $item;

	/**
	 * @ORM\Column(type="string", length=1)
	 *
	 */
	protected $status;

	/**
	 * @ORM\Column(type="datetime", name="due_date")
	 *
	 * @Assert\NotBlank()
	 * @Assert\Type("\DateTime")
	 */
    protected $dueDate;


	/**
	 * Initializes an instance
	 */
	public function __construct()
	{
		$this->initialize();

		return $this;
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set item
     *
     * @param string $item
     * @return Item
     */
    public function setItem($item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return string 
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Item
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set dueDate
     *
     * @param \DateTime $dueDate
     * @return Item
     */
    public function setDueDate($dueDate)
    {
        $this->dueDate = $dueDate;

        return $this;
    }

    /**
     * Get dueDate
     *
     * @return \DateTime 
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }

	/**
	 * Initializes an instance
	 */
	public function initialize()
	{
		$this->setItem('');
		$this->setStatus('O');
		$this->setDueDate(new \DateTime('tomorrow'));
	}

}
