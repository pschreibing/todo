<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TodoControllerTest extends WebTestCase
{
	/**
	 * @dataProvider urlProvider
	 *
	 * @param $url
	 */
	public function testPageIsSuccessful($url)
    {
	    $client = self::createClient();
	    $client->request('GET', $url);

	    $this->assertTrue($client->getResponse()->isSuccessful());
    }

	/**
	 * @return array
	 */
	public function urlProvider()
	{
		return array(
			array('/sf2todo/web/'),
			array('/sf2todo/web/edititem/0'),
			array('/sf2todo/web/removeitem/0'),
		);
	}


}
